import Vue from 'nativescript-vue'
Vue.registerElement('HTMLLabel', () => require('@nativescript-community/ui-label').Label);
import Home from './components/Home'
Vue.config.silent = false
new Vue({
  render: (h) => h('frame', [h(Home)]),
}).$start()
